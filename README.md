# __Training-Docker-Jenkins__

## __1. Docker101__

เข้า Folder 1-hello-world ด้วยคำสั่ง  
```
cd 1-hello-world
```
สั่ง Docker build image ตามคำสั่งดังนี้

```
docker build -t hello-world .
```

> -t หมายถึง tag เป็นการตั้งชื่อ และ tag ให้ docker image

ดู image ที่สร้างด้วยคำสั่ง
```
docker images
```

ดู History ที่สร้างด้วยคำสั่ง
```
docker history bash
docker history hello-world:latest
```

จะพบว่า Docker Image History ของ hello-word Image นั้นเกิดมาจาก History ของ Bash Image และคำสั่งเพิ่มเติมในตัว Dockerfile ใน hello-word Image เอง


ซึ่งคำสั่ง History จะทำให้เราสามารถแกะย้อนกลับไปได้ว่าแต่ล่ะ Image เกิดมาจากคำสั่งอะไรบ้าง และทราบถึงจำนวน space ที่ใช้ไปครับ

__สร้าง Docker Container จาก Docker Image__  
```
docker run hello-world
```

__ดู Container ที่สร้างด้วยคำสั่ง__
```
docker ps -a
```
> -a คือการเรียกดู container ทั่งหมด เราจะเห็นว่า container ที่ทำงานและ หยุดทำงานไปแล้ว

__ลบ Container ด้วยคำสั่ง__
```
docker stop <container_name|container_id>
docker rm <container_name|container_id>
```
> ต้อง stop container ก่อนลบ

## __2. Pass Parameter__
ศึกษาการเขียน Docker File ร่วมกับ Bash Script โดยจะทดลองแสดงผลของ Environment Variable และศึกษาว่าเราจะทำการรัน Docker Image ยังไงให้สามารถส่งค่า Environment Variable เข้าไปด้วยได้

เข้าไปที่ 2-pass-param ด้วยคำสั่ง
```
cd 2-pass-param
```
สั่ง Docker build image ตามคำสั่งดังนี้
```
docker build -t passparameter .
```
จะพบว่ามี Image ที่เพิ่มมา Image ใหม่อันเดียว คือ 2.passparameter แต่ตัว image bash นั้นเพราะว่าการ Pull มาแล้วเลยใช้ตัวเดิมต่อได้เลยครับ

รัน Image ด้วยคำสั่งดังนี้
```
docker run --rm passparameter
docker run --rm -e MESSAGE="Hello World from Environment" passparameter
```
> --rm จำทำให้ container นั่น ถูกลบโดยทีนทีหลังจากที่ทำงานเสร็จ  
-e คือการประกาศ Environment

จะพบว่าคำสั่งแรกนั้นจะไม่มีผลลัพธ์อะไรออกมาที่ Terminal เพราะว่าเรายังไม่ได้ Set Environment Variable MESSAGE

ส่วนคำสั่งที่สองคือการรัน Image พร้อมกับทำการ Set Environment Variable MESSAGE ไปด้วย ทำให้มีข้อความแสดงผลออกมาที่ Terminal

## __3. HTML Container__
เข้าไปที่ 3-html-container ด้วยคำสั่ง
```
cd 3-html-container
```
สั่ง Docker build image ตามคำสั่งดังนี้
```
docker build -t web1 .
```
รัน Image ด้วยคำสั่งดังนี้
```
docker run -d -p 8080:80 --name web web1
```
> -d คือการสั่งรัน background  
-p คือการ map port  
-- name คือ ประกาศชื่่อให้ container ถ้าไม่ประกาศมันจะ random ชื่อให้

หลังจากนั้นลองเข้า Web browser ที่ [web](http://localhost:8080)

## __3.1 Docker compose__
หยุดการทำงาน web container ด้วยคำสั่ง
```
docker stop web1
```
สร้าง ไฟล์ docker-compose.yml
```
version: "3"
services:
  web:
    image: nginx:1.16.0-alpine
    build: .
    ports: 
      - "8081:80"
    volumes: 
      - "./venus:/var/www"
      - "./nginx.conf:/etc/nginx/nginx.conf"
    entrypoint: ["nginx", "-g", "daemon off;"]
```
สั่ง run docker-compose
```
docker-compose up -d
```
เมื่อต้องการหยุดทำงาน ใช้คำสั่ง
```
docker-compose down
```

## __EX. create web Container__
- สร้าง web อะไรก็ได้
- เขียน docker-compose สำหรับสร้าง image
- volumes web เข้าไปใน image
- run container โดย กำหนด port ขาออก 3001
- ลองแก้ไขข้อมูลบน web โดยที่ไม่ต้อง start container ใหม่
